// ----------------------------------------------------------------------------
// OBJECTS
// ----------------------------------------------------------------------------

function ContentItem(id, url, title, content, imageString, pending) {
	this.id = id;
    this.url = url;
	this.title = title;
	this.content = content;
	this.imageString = imageString;
	this.pending = pending;
}

function cloneContentItem(newContentItem, clonedContentItem) {
	newContentItem.id = clonedContentItem.id;
    newContentItem.url = clonedContentItem.url;
	newContentItem.title = clonedContentItem.title;
	newContentItem.content = clonedContentItem.content;
	newContentItem.imageString = clonedContentItem.imageString;
	newContentItem.pending = clonedContentItem.pending;
}

// ----------------------------------------------------------------------------
// LOCAL STORAGE METHODS
// ----------------------------------------------------------------------------

// Clear all our saved key/value pairs in localStorage
function clearItems() {	
	localStorage.clear();
}

// Save a key/value pair to localStorage
function setItem(key, value) {    
	localStorage.setItem(key, JSON.stringify(value));
}

// Get a value from localStorage with the given key
function getItem(key) {
	return localStorage.getItem(key);
}

// Concatenate all values from localStorage into a string
function getAllItems() {    
	var all = "";
	
	for (var i = 0; i < localStorage.length; i++)
		all += localStorage.getItem(localStorage.key(i)) + "\n";
	
	return all;
}

// Remove a value from localStorage with the given key
function removeItem(key) {    
	localStorage.removeItem(key);
}

// ----------------------------------------------------------------------------
// Save a ContentItem to our array. If the url is unique, add it, otherwise update it.
function saveContentItem(contentItem) {
		
	// Get the ContentItem collection
	var contentItemArray = JSON.parse(getItem('contentItemArray'));
	var count = contentItemArray.length;

	// Iterate through the collection
	for (var i = 0; i < count; i++) {
		
		// If we have a matching url, update the ContentItem
		if (contentItemArray[i].url == contentItem.url) {
			cloneContentItem(contentItemArray[i], contentItem);			
			setItem('contentItemArray', contentItemArray);

			return;
		}		
	}
	
	// No matching url, update the ContentItem
	contentItemArray.push(contentItem);	
	setItem('contentItemArray', contentItemArray);
}

// Get a ContentItem from the array that contains the given url
function getContentItem(url) {
	
	// Get the ContentItem collection
	var contentItemArray = JSON.parse(getItem('contentItemArray'));
	var count = contentItemArray.length;
	
	// Iterate through the collection
	for (var i = 0; i < count; i++) {
		
		// If we have a matching url, update the ContentItem
		if (contentItemArray[i].url == url) {
			return contentItemArray[i];
		}		
	}
	
	return null;
}

// Remove a ContentItem from our array if its url matches the passed url
function removeContent(id) {
	
	// Get the ContentItem collection
	var contentItemArray = JSON.parse(getItem('contentItemArray'));
	var count = contentItemArray.length;
	
	// Iterate through the collection
	for (var i = 0; i < count; i++) {
		
		// If we have a matching url, remove the ContentItem
		if (contentItemArray[i].id == id) {			
			contentItemArray.splice(i, 1);
			setItem('contentItemArray', contentItemArray);

			break;
		}		
	}
}

// Clear all ContentItems
function clearAllContent() {
	
	// Get the ContentItem collection
	var contentItemArray = JSON.parse(getItem('contentItemArray'));
	var count = contentItemArray.length;
	
	// Iterate through the collection
	for (var i = 0; i < count; i++) {
		removeContent(contentItemArray[i].id);
	}
}

// Concatenate an easily readable string of all ContentItems
function getAllContent() {
	
	var items = "";
	
	// Get the ContentItem collection
	var contentItemArray = JSON.parse(getItem('contentItemArray'));
	var count = contentItemArray.length;
	
	// Iterate through the collection
	for (var i = 0; i < count; i++) {
		var contentItem = contentItemArray[i];
		
		items += "- ContentItem -\n";
		items += "Index: " + i + "\n";
        items += "ID: " + contentItem.id + "\n";
		items += "URL: " + contentItem.url + "\n";
		items += "Title: " + contentItem.title + "\n";
		items += "Content: " + contentItem.content.substring(0, 30) + "...\n\n";
	}
	
	return items;
}