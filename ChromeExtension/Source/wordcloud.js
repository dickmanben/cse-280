var key = [];
var count = [];
var sort = [];
var style = getItem("Settings");

populate();

// Populate the word cloud list on the popup
function populate() {
    
    // Get the ContentItem collection
    var contentItemArray = JSON.parse(getItem('contentItemArray'));
    var count = contentItemArray.length;
	
	// Display message if no content
	if (count == 0) {
		$("#wordCloudList").append("<p style='font-weight: bold; font-size: 11pt; text-align: center;'>No Items</p>");
	}
	else {
		
		// Create the clear button
		var clearBtnStyle = "width: 100%; border: 1px solid gray; margin: 5px 0 15px 0; padding: 5px 0; font-weight: bold; font-size: 12pt;";		
		$("#wordCloudList").append("<input type='button' id='clearBtn' value='Clear All Items' style='" + clearBtnStyle + "'></input>");
		
		// Clear button click handler
		$("#clearBtn").click(function() {
			clearAllContent();
			refreshPopup();
		});
		
		// Iterate through the collection
		for (var i = 0; i < count; i++) {	
			
			var id = contentItemArray[i].id;
			var url = contentItemArray[i].url;
			var title = contentItemArray[i].title;
			var content = contentItemArray[i].content;
			var imageString = contentItemArray[i].imageString;
			var pending = contentItemArray[i].pending;       

			var linkStyle = "text-decoration: none; font-size: 11pt;";
			
			$("#wordCloudList").append("<a href='" + url + "' target='_blank' style='" + linkStyle + "'>" + title + "</a><br>");

			// Only show the word cloud if we are using the word cloud style
			if (style == 1 || style == null) {
				
				// Show a Generating... message if it is currently being created
				if (pending) {
					var divStyle = "border: 1px solid gray; padding: 5px; margin: 5px 0;";
					$("#wordCloudList").append("<div style='" + divStyle + "'><b>Generating Word Cloud...</b></div>");	
				}
				else {

					var divID = 'div' + id;
					var btnID = 'btn' + id;

					var divStyle = "display: inline-block; border: 1px solid gray; margin: 5px 0;";
					var btnStyle = "display: inline-block; margin: 5px 0 0 5px; vertical-align: top; border: 1px solid gray; font-size: 13pt; background-color: white;";

					$("#wordCloudList").append("<div id='" + divID + "' style='" + divStyle + "'>" + imageString + "</div>");
					$("#wordCloudList").append("<input type='button' id='" + btnID + "' value='&times' style='" + btnStyle + "'></input>");

					// Click handler for close button to remove the item from the list
					var closeButton = document.getElementById(btnID);
					closeButton.onclick = (function (i) {
						return function() {
							removeContent(i);                
							refreshPopup();
							return false;
						}				
					})(id);
				}
			}            			
			else if (style == 2) {
				farmer(content);
			}
			
		}
	}
    
}

// Generate the word cloud
function generate(url, content) {

	var fill = d3.scale.category20();
	var font = getItem("font");
	parse(content);
	//parseIndividual();
	var txtsplit = key;
	var pageHtml = $('html').html();
	
	// Strip off any existing <svg> tag to prevent base-64 encoding from appending the old <svg> image to the new one
	var svgStartIndex = pageHtml.indexOf("<svg");	// Not "<svg>" since the tag does not have a closing bracket until the end of the data
	var svgEndIndex = pageHtml.indexOf("</svg>");

	// Make sure we don't call substring() with -1 for indices (when the page has no <svg> tags)
	if (svgStartIndex >= 0 && svgEndIndex >= 0) {	
	
		svgEndIndex += 6;	// Add 6 to the end index to account for the length of the tag
		var svgString = document.body.innerHTML.substring(svgStartIndex, svgEndIndex);
		document.body.innerHTML = document.body.innerHTML.replace(svgString, "");
	}

	// Set up the d3 cloud generator
	d3.layout.cloud()
		.size([300, 300])
		.timeInterval(10)	// Makes the call asynchronous
		.padding(5)
		.rotate(function() { return ((Math.floor(Math.random() * 7)) - 3) * 30; })
		.font(font)
		.fontSize(function(d) { return d.size; })
		.on("end", draw)
		.words(txtsplit.map( function(d) {
			return {text: d, size: 10 + Math.random() * 40};
		}))
		.start();
		
	// Draw the word cloud
	function draw(words) {
		var width = 300;
		var height = 300;
		
		d3.select("body").append("svg")
			.attr("width", width)
			.attr("height", height)
		.append("g")
			.attr("transform", "translate(150,150)")
		.selectAll("text")
        .data(words)
		.enter().append("text")
			.style("font-size", function(d) { return d.size + "px"; })
			.style("font-family", font)
			.style("fill", function(d, i) { return fill(i); })
			.attr("text-anchor", "middle")
			.attr("transform", function(d) {
				return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			})
        .text(function(d) { 			
			return d.text.trim(); 
		});
		
		// Get the html to convert to an image			
		var svgData = d3.select("svg")
			.attr("version", 1.1)
			.attr("xmlns", "http://www.w3.org/2000/svg")
			.node().parentNode.innerHTML;
			
		// Strip off anything before the <svg> tag
		var svgIndex = svgData.indexOf("<svg");
		svgData = svgData.substring(svgIndex);
		svgData = unescape(encodeURIComponent(svgData));	// Remove UNICODE characters

		// Get the base-64 encoded string for the image
		var imageSrc = 'data:image/svg+xml;base64,' + btoa(svgData);	// btoa = encode string in base 64		
		var imageString = '<img src=\"' + imageSrc + '\"></img>';		
		
		// Get the current ContentItem based on the url
		var contentItem = getContentItem(url);			

		// Set the ContentItem's image and pending status
		contentItem.imageString = imageString;
		contentItem.pending = false;

		// Save the ContentItem
		saveContentItem(contentItem);
        delete imageString;
        delete imageSrc;
        delete svgIndex;
        
        refreshPopup();                
	}
}

function parse(input) {
    delete key;
    var word = input.toUpperCase();
    var b;
    word = word.split(" ");
    var parseWords = ["  ","\t", "A", "AN", "THE", "AND", "OR", "NOR", "IS","    ", "AM", "ARE", "WAS", "WERE", "BE", "BEING", "BEEN", "BUT", "NEITHER", "EITHER", "OF", "IF", "IN", "IT", "THAT", "FOR", "I", "BECAUSE", "HAVE", "TO", "TOO", "YOU", "HE", "WITH", "ON", "DO", "SAY", "SAYS", "THIS", "THEY", "AT", "BUT", "WE", "HIS", "FROM", "NOT", "BY", "SHE", "OR", "AS", "WHAT", "GO", "THEIR", "THERE", "CAN", "WHO", "GET", "WOULD", "ALL", "HER", "HE", "ABOUT", "MY", "MAKE", "KNOW", "WILL", "UP", "THERE", "THEY'RE", "DON'T", "ITS", "IT'S", "THINK", "SO", "THOUGH", "WHEN", "WHICH", "THEM", "US", "OUT", "TAKE", "INTO", "JUST", "SEE", "HIM", "HER'S", "THEIR'S", "DON'T", "ISN'T", "CAN'T", "CAN", "HAVEN'T", "SHOULDN'T", "WOULD'VE", "SHOULD'VE", "COULD'VE", "GONE", "~", "`",  "\"", "'", "©", "®", "™", "+", "=", "-", "–", "", "   ", "  ", "    ", " ", "_", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "<", ">", ",", ".", "/", "\\", "?", ";", ":", "»", "[", "]", "{", "}", "|", "[]", "{}", "()", "<>", "});"];

    for (var i = 0; i < word.length; i++){
        b = false;
        for (var x = 0; x < parseWords.length; x++){
            if (word[i] == parseWords[x]){
                delete word[i];
                //alert("word deleted");
                b = true;
                break;
            }
        }
		var testArray = [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "0","~", "`",  "\"", "'", "©", "®", "™", "+", "=", "-", "–", "", "   ", "  ", "    ", " ", "_", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "<", ">", ",", ".", "/", "\\", "?", ";", ":", "»", "[", "]", "{", "}", "|", "[]", "{}", "()", "<>", "});"];
		var pilot = word[i];
		if(pilot != null){
			for(var h = 0; h < pilot.length; h++){
				for(var y = 0; y < testArray.length; y++){
					if(pilot[h] == testArray[y]){
						//alert(pilot + "Start");

						//alert(pilot);
						pilot = pilot.replace(/[^a-zA-Z ]/g, "");
						pilot = pilot.replace(/[.]/g, "");
						//alert(pilot);
						word[i] = pilot;
						break;
					}
				}
			}
		}
        while(!b){

			var keylength = key.length;
            for(var j = 0; j < keylength; j++){
                if (word[i] == key[j]){
                    count[j] = count[j] + 1;
                    b = true;
                }
            }
            if(!b){
                key[keylength + 1] = word[i];
                count[keylength + 1] = 1;
                b = true;
            }
        }
    }
    return key.join(" ");
}


function farmer(b){
    //alert("sort is called");
    parse(b)
    var arr = count;
    var maxIndex = 0;
    var x = 0;
    var hold = [];
    while(x < 10){
     var max = 0;
    for (var i = 0; i < arr.length; i++) {
        if(arr[i] == null){
            continue;
        }
        else if(arr[i] > max && key[i] != null) {
            maxIndex = i;
            max = arr[i];
            }
        }
    hold[x] = arr[maxIndex];
    delete arr[maxIndex];
    sort[x] = maxIndex;
    x++;
    }
    var y = 0;
    while(y < sort.length){
        //alert(sort[y]);
        var sucks = sort[y];
        //alert("Word: " + key[sucks] + ".    Count:  " + hold[y]);
        $("#wordCloudList").append("<h3>" + "Count: " + hold[y] + "   Word: "  + key[sucks] + "</h3>");
        y++;
    }
}

function list(a, b){
    //alert("called");
    parse(b);
    //alert("parse called");
    //alert(count);
    farmer(count);
}

// Refresh the popup
function refreshPopup() {
    
    // Repopulate the word cloud list on the popup
    var popups = chrome.extension.getViews({type: 'popup'});
    popups[0].location.reload(true);
}


function parseIndividual(){
	
	var x = 0;
	var testArray = [ "~", "`",  "\"", "'", "©", "®", "™", "+", "=", "-", "–", "", "   ", "  ", "    ", " ", "_", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "<", ">", ",", ".", "/", "\\", "?", ";", ":", "»", "[", "]", "{", "}", "|", "[]", "{}", "()", "<>", "});"];
	while(x < key.length){
		var hold = key[x];
		if(hold != null){
			for(var i = 0; i < hold.length; i++){
				for(var y = 0; y < testArray.length; y++){
					if(hold[i] == testArray[y]){
						hold[i] = "";
						break;
					}
				}
			}
		}
		key[x] = hold;
	}
}
		
