/*	TODO:
 * 	1) Improve content parser (try json.parse, html())
 *  2) Fix bug where converting a pending word cloud image will fail if another one after finishes generating first (encodes data for both images)
 */

// ----------------------------------------------------------------------------
// GLOBAL VARIABLES
// ----------------------------------------------------------------------------

var getContent = false;
var matchTags = 'body, title, p, li, h1, h2, h3, h4, h5, h6, div, table, tbody, thead, th, tr, td';
var style = getItem("Settings");
// ----------------------------------------------------------------------------
// MAIN BODY CODE
// ----------------------------------------------------------------------------

$(document).ready(function () {    
    
	// Create the context menu
	var parent = chrome.contextMenus.create({
	    "title": "Open with StratoView",
	    "contexts": ['link'],
	    "onclick" : function onClick(OnClickData) {
	       
			var url = decodeURIComponent(OnClickData.linkUrl);
			var index = -1;
		   
			// Handle search engine redirects
			if (url.indexOf("=http") > -1) {
				index = url.indexOf("=http");	// Find index of url 
				url = url.substring(index+1);	// Strip text before url 
			}		
			if (url.indexOf("&") > -1) {		// If there are any remaining parameters after url, find the end of url parameter
				index = url.indexOf("&");
				url = url.substring(0, index);
			}

			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {		
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
					   
						// Get our required fields
                        var id = getItem('contentID');
						var title = $(xhr.responseText).filter('title').text();
						var content = $(xhr.responseText).filter(matchTags).map(function() { 	// NEEDS IMPROVEMENT
						   return $.text(this);
						}).get().join(" ");
						
						// Save the ContentItem with an empty image and pending word cloud generation
						var contentItem = new ContentItem(id, url, title, content, null, true);
						saveContentItem(contentItem);

                        id++;
                        
                        // Increment the contentID
                        setItem('contentID', id);                    

						// Generate word cloud, pass the url for reference

                        //alert(style);
						if(style == 1 || style == null){
                        //alert("generate");
                        generate(url, content);
                        }
                        if(style == 2){
                        //alert("called in bg");
                        list(url, content);   
                        }
						//alert(html);
						
						// // Get the base-64 encoded string for the image
						// var imageSrc = 'data:image/svg+xml;base64,' + btoa(html);	// btoa = encode string in base 64		
						// var imageString = '<img src=\"' + imageSrc + '\"></img>';						
		
						// // Set the ContentItem's image and pending status
						// contentItem.imageString = imageString;
						// contentItem.pending = false;

						
						// alert(contentItem.imageString);

					}
					else {
					   alert(xhr.statusText + "test");
					}
				}
			}
			xhr.open("GET", url, true);
			xhr.send();
	    }
	});

	// Clear all local storage
    clearItems();    
	
	// Initialize our ContentItem array and save it to local storage
	var contentItemArray = [];
	setItem('contentItemArray', contentItemArray);
    setItem('contentID', 0);
	setItem('font', 'Impact');

	//alert(getAllContent());
});
